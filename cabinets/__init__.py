from __future__ import unicode_literals

__author__ = 'Roberto Rosario'
__build__ = 0x010101
__copyright__ = 'Copyright 2017 Roberto Rosario'
__license__ = 'Apache 2.0'
__title__ = 'mayan-cabinets'
__version__ = '1.1.1'

default_app_config = 'cabinets.apps.CabinetsApp'
