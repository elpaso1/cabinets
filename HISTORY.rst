1.1.1 (2017-03-09)
==================

- Fix typo that removed the jstree dist folder
  Multi level widget should render correctly now

1.1.0 (2017-02-07)
==================

- Fix cabinet list API endpoint.
- Improve cabinet document API endpoints.
- Add more API tests.

1.0.0 (2017-01-25)
==================

- Initial release.
